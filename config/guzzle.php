<?php
/**
 * Created by PhpStorm.
 * User: frederick.farfan
 * Date: 11/2/2019
 * Time: 10:27
 */

return [
    'base_uri' => 'https://api.spotify.com/v1/playlists/',
    'operations' => [
        'tracks' => [
            'method' => 'GET',
            'name' => 'tracks'
        ]
    ]
];