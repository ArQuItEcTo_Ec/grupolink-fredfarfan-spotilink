<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>@yield('title') | {{ env('APP_NAME') }} | Frederick Farfán J. </title>
    @include('layouts.include.master-style')
</head>
<body class="grey lighten-4">
@include('layouts.include.master-header')
<main>
    @yield('main')
</main>
@include('layouts.include.master-footer')
@include('layouts.include.master-script')
</body>
</html>
