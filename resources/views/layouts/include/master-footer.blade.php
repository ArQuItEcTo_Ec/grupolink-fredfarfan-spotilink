<footer class="page-footer grey darken-4 no-pad-top">
    <div class="row no-margin">
        <div class="footer-copyright">
            <div class="col s12 l6">
                <p class="no-margin">
                    <span id="copyright"><i class="fa fa-copyright"></i> 2019 &#8211; <span class="grey-text text-lighten-4 bold">Frederick Farfán J.</span></span>
                </p>
            </div>
            <div class="col s12 l6">
                <div id="share-links" class="right-align white-text">
                    <a href="https://www.facebook.com/frederick.farfan" target="_blank"><i class="fa fa-facebook" title="Comparte!"></i></a>
                    <a href="https://twitter.com/FredFarfanJ" target="_blank"><i class="fa fa-twitter" title="Sígueme!"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>