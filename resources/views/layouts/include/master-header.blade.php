<header>
    <nav class="fixed" role="navigation">
        <div class="nav-wrapper">
            <a id="nav-logo-container" href="#" class="brand-logo hide-on-large-only"></a>



            @include('layouts.include.master-sidenav')
        </div>
        @yield('tabs')
    </nav>
</header>