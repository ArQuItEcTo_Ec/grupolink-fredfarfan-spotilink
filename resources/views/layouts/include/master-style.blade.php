{!! HTML::style('https://fonts.googleapis.com/icon?family=Material+Icons') !!}
{!! HTML::style('css/font-awesome.min.css') !!}
{!! HTML::style('css/materialize.min.css') !!}
{!! HTML::style('css/style.css?ver=1.1') !!}
{!! HTML::style('css/multi.css?ver=1.1') !!}
{!! HTML::script('js/jquery.min.js') !!}
@yield('style')