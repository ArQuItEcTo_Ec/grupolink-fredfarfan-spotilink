<ul id="nav-mobile" class="sidenav sidenav-fixed">
    <li class="logo hide-on-med-and-down" style="text-align: center;">
        <a id="logo-container" href="/" class="brand-logo"></a>
    </li>
    <li class="no-hover"><p class="more-text no-padding no-margin-bot grey-text truncate">Mi cuenta</p></li>
    <li class="active"><a class="waves-light truncate" href="#"><i class="material-icons">queue_music</i>Mis canciones preferidas</a></li>

</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>