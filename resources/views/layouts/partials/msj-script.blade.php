@if(session('welcome'))
    M.toast({html: '<p><i class="fa fa-smile-o toast-i yellow-text" style="margin-right: 0.5rem"></i><span>{!! session()->pull('welcome') !!}</span></p>', displayLength: 10000, classes: 'white-text'});
@endif
@if(session('msj_success'))
    console.log('{!! session('msj_success') !!}');
    M.toast({html: '<p><i class="fa fa-check toast-i yellow-text" style="margin-right: 0.5rem"></i><span>{!! session()->pull('msj_success') !!}</span></p>', displayLength: 10000, classes: 'white-text'});
@endif
@if(session('msj_warning'))
    console.log('{!! session('msj_warning') !!}');
    M.toast({html: '<p><i class="fa fa-info toast-i orange-text text-darken-4" style="margin-right: 0.5rem"></i><span>{!! session()->pull('msj_warning') !!}</span></p>', displayLength: 10000, classes: 'yellow lighten-3 grey-text text-darken-4'});
@endif
@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        M.toast({html: '<p><i class="fa fa-exclamation-circle toast-i" style="margin-right: 0.5rem"></i><span>{!! $error !!}</span></p>', displayLength: 10000, classes: 'red darken-4 white-text'});
        console.log('{!! preg_replace("/[\n|\r|\n\r]/i", ' ', $error) !!}');
    @endforeach
@endif