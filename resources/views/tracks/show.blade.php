@extends('layouts.master')

@section('title')
    Mis playlists
@stop

@section('main')
    <section id="front-wrapper" class="parallax-container valign-wrapper">
        <div class="parallax"><img src="{{asset('img/parallax/best-music.jpg')}}"></div>
        <div class="valign">
            <h3 class="no-margin"><a href="#" class="white-text">Mis canciones preferidas</a></h3>
            <h6 style="text-shadow: 2px 2px 2px #424242;">Tracks que se repiten entre mis playlists</h6>
        </div>
    </section>

    <section class="row" style="margin-top: 2em">
        <div class="container-2 white z-depth-1">
            <div class="row no-margin">
                <table id="data-table" class="centered highlight responsive-table hide col s12">
                    <thead>
                    <tr>
                        <th>Álbum</th>
                        <th>Nombre</th>
                        <th>Artistas</th>
                        <th>URL</th>
                    </tr>
                    </thead>

                    <tbody id="data-content">

                    </tbody>
                </table>
                <div id="content-loading" class="center" style="padding: 2em"></div>
            </div>
        </div>
    </section>

@stop

@section('script')
    <script>
        $.ajax({
            url: "{{ route('api.v1.tracks.index', 'duplicates') }}",
            type: "GET",
            beforeSend: function () {
                $('#content-loading').html(loadingString);
            },
            complete: function(){
                $('#content-loading').empty();
                $('#data-table').removeClass('hide');
            },
            success: function (result) {
                console.log(result);
                let tracks = result.data;
                if(!tracks.length) {
                    $('#data-content').append('<tr><td>-</td><td>-</td><td>-</td><td>-</td></tr>');
                } else {
                    tracks.forEach(function (track) {
                        let track_img = null;
                        track.imgs.forEach(function (img) {
                            if(!track_img)
                                track_img = img.url;
                            if(img.width === 64)
                                track_img = img.url;
                        });
                        let track_artist = "";
                        track.artists.forEach(function (artist) {
                            track_artist += '<a href="' + artist.url + '" target="_blank">' + artist.name + '</a><br>';
                        });
                        $('#data-content').append('<tr>\n' +
                            '                        <td><img src="' + track_img + '" alt=""></td>\n' +
                            '                        <td>' + track.name + '</td>\n' +
                            '                        <td>' + (track_artist ? track_artist : '-') + '</td>\n' +
                            '                        <td><a class="green-text text-darken-2" href="' + track.url + '" target="_blank"><i class="material-icons">music_note</i></a></td>\n' +
                            '                    </tr>');
                    });
                }
            },
            error: function(errors){
                console.error(errors);
                flash__msg('Ocurrió un error al intentar eliminar el familiar con capacidad especial.', false);
            }
        });
    </script>
@stop