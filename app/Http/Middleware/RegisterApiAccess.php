<?php

namespace App\Http\Middleware;

use App\Repositories\Store\Logs\LogApiAccess;
use Closure;

class RegisterApiAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        LogApiAccess::create([
            'ip' => $request->ip(),
            'method' => $request->method(),
            'resource' => $request->route()->getPrefix(),
            'operation' => $request->route()->getActionMethod(),
        ]);
        return $next($request);
    }
}
