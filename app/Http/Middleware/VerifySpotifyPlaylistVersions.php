<?php

namespace App\Http\Middleware;

use App\Models\PlaylistVersion;
use App\Repositories\Traits\UpdatesSpotifyPlaylists;
use Closure;

class VerifySpotifyPlaylistVersions
{
    use UpdatesSpotifyPlaylists;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $last_version = PlaylistVersion::latest()->first();
        // Si no ha sido creada una versión de la playlist, actualizo
        if(!$last_version)
            $this->do_update();
        else {
            // Si ha pasado el tiempo mímino entre ahora y la última actualización de la versión, actualizo
            if(abs(now()->diff($last_version->created_at)->format('%i')) > env('API_UPDATE_DELAY_MINUTES')) {
                $this->do_update();
            }
        }
        return $next($request);
    }
}
