<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Playlist as PlaylistResource;
use App\Http\Resources\Track as TrackResource;
use App\Http\Resources\Track;
use App\Models\Playlist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlaylistController extends Controller
{
    public function index()
    {
        return PlaylistResource::collection(Playlist::all());
    }

    public function tracks(Request $request, Playlist $playlist)
    {
        return TrackResource::collection($playlist->tracks);
    }
}
