<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Artist as ArtistResource;
use App\Http\Resources\Track as TrackResource;
use App\Models\Artist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArtistsController extends Controller
{
    public function index()
    {
        return ArtistResource::collection(Artist::orderBy('name')->get());
    }

    public function tracks(Request $request, Artist $artist)
    {
        return TrackResource::collection($artist->tracks);
    }
}
