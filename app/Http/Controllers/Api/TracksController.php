<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Track as TrackResource;
use App\Models\Track;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TracksController extends Controller
{
    public function index(Request $request, $option = null)
    {
        if($option == 'duplicates')
            $tracks = Track::join('ctr_playlists__tracks', 'ctr_tracks.id', '=', 'ctr_playlists__tracks.id_track')
                ->groupBy('ctr_tracks.id', 'ctr_tracks.id_str', 'ctr_tracks.name', 'ctr_tracks.url')
                ->havingRaw('count(*) > 1')
                ->select('ctr_tracks.id', 'ctr_tracks.id_str', 'ctr_tracks.name', 'ctr_tracks.url')
                ->orderBy('ctr_tracks.name')
                ->get();
        else
            $tracks = Track::orderBy('name')->get();
        return TrackResource::collection($tracks);
    }
}
