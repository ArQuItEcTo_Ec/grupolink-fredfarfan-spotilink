<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TrackImg extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->resource) {
            return [
                'width' => $this->width,
                'height' => $this->height,
                'url' => $this->url
            ];
        }
        return [];
    }
}
