<?php

namespace App\Http\Resources;

use App\Http\Resources\Artist as ArtistResource;
use App\Http\Resources\TrackImg as TrackImgResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Track extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->resource) {
            return [
                'id_str' => $this->id_str,
                'name' => $this->name,
                'url' => $this->url,
                'artists' => ArtistResource::collection($this->artists),
                'imgs' => TrackImgResource::collection($this->imgs)
            ];
        }
        return [];
    }
}
