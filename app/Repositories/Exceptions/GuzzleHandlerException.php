<?php
/**
 * Created by PhpStorm.
 * User: frederick.farfan
 * Date: 11/2/2019
 * Time: 11:03
 */

namespace App\Repositories\Exceptions;

use Exception;
use Throwable;

class GuzzleHandlerException extends Exception
{
    /**
     * Construct the exception. Note: The message is NOT binary safe.
     * @link https://php.net/manual/en/exception.construct.php
     * @param string $message [optional] The Exception message to throw.
     * @param int $code [optional] The Exception code.
     * @param Throwable $previous [optional] The previous throwable used for the exception chaining.
     * @since 5.1.0
     */
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}