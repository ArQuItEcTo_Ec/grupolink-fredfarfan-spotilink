<?php

namespace App\Repositories\Store\Logs;

use Illuminate\Database\Eloquent\Model;

class LogApiAccess extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log__api_access';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ip',
        'method',
        'resource',
        'operation'
    ];
}
