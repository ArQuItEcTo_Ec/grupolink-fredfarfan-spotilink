<?php
/**
 * Created by PhpStorm.
 * User: frederick.farfan
 * Date: 11/2/2019
 * Time: 11:06
 */

namespace App\Repositories\Traits;

use App\Models\TrackImg;
use Exception;
use App\Models\Artist;
use App\Models\Playlist;
use App\Models\PlaylistTrack;
use App\Models\PlaylistVersion;
use App\Models\Track;
use App\Models\TrackArtist;
use App\Repositories\Exceptions\GuzzleHandlerException;
use App\Repositories\Handlers\GuzzleHandler;

trait UpdatesSpotifyPlaylists
{
    public function do_update()
    {
        //Creo una nueva versión de la base
        $version = PlaylistVersion::create();
        //Leo todas las listas
        $playlists = Playlist::all();
        $guzzle = new GuzzleHandler();
        $all = collect();
        foreach ($playlists as $playlist) {
            try {
                //Cargo las canciones por lista
                $result = json_decode($guzzle->callPlaylistTracks(env('API_BEARER_TOKEN'), $playlist->id_str));
                foreach ($result->items as $item) {
                    $track = Track::where('id_str', $item->track->id)->first();
                    // En caso de no existir la canción, la guardo con todos sus datos (artistas, imágenes, etc)
                    if(!$track) {
                        $arr = json_decode(json_encode( $item->track->external_urls), true);
                        $track = Track::create(['id_str' => $item->track->id, 'name' => $item->track->name, 'url' => key_exists('spotify', $arr) ? $arr['spotify'] : null]);
                        // Guardo artistas
                        foreach ($item->track->artists as $artist) {
                            $arr = json_decode(json_encode($artist->external_urls), true);
                            $artist = Artist::where('id_str', $artist->id)->first() ?? Artist::create(['id_str' => $artist->id, 'name' => $artist->name, 'url' => key_exists('spotify', $arr) ? $arr['spotify'] : null]);
                            $ta = TrackArtist::where('id_artist', $artist->id)->where('id_track', $track->id)->first() ?? TrackArtist::create(['id_artist' => $artist->id, 'id_track' => $track->id]);
                        }
                        // Guardo las imágenes del album y las asigno a la canción.. Por qué?? Para que la tabla tenga algo de color, obviamente
                        foreach ($item->track->album->images as $image) {
                            TrackImg::create([
                                'id_track' => $track->id,
                                'width' => $image->width,
                                'height' => $image->height,
                                'url' => $image->url
                            ]);
                        }

                    }
                    $pt = PlaylistTrack::where('id_playlist', $playlist->id)->where('id_track', $track->id)->first();
                    if($pt) {
                        //Actualizo la versión de la canción de la pista, en caso de existir
                        $pt->id_version = $version->id;
                        $pt->save();
                    } else {
                        //O la creo, en caso que no
                        $pt = PlaylistTrack::create(['id_playlist' => $playlist->id, 'id_track' => $track->id, 'id_version' => $version->id]);
                    }
                    $all->push($track);
                }
            } catch (GuzzleHandlerException $e) {
                session()->flash('msj_error', 'Se presentó un error al momento de cargar la información de Spotify: SPx'.$e->getMessage());
            } catch (Exception $e) {
                session()->flash('msj_error', 'Se presentó un grave error al momento de cargar la información de Spotify.');
            }
        }
        //Finalmente, elimino todos los tracks que no estén en esta versión.. Es decir, que fueron eliminados de la playlist
        $tracks_not_valid = PlaylistTrack::where('id_version', '<>', $version->id)->get()->each->delete();
    }
}