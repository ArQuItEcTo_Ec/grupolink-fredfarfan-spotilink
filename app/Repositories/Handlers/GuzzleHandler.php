<?php
/**
 * Created by PhpStorm.
 * User: frederick.farfan
 * Date: 11/2/2019
 * Time: 10:33
 */

namespace App\Repositories\Handlers;

use App\Repositories\Exceptions\GuzzleHandlerException;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;

class GuzzleHandler
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => config('guzzle.base_uri')]);
    }

    public function callPlaylistTracks($bearer_token, $id_playlist)
    {
        //try {
            $response = $this->client->request(config('guzzle.operations.tracks.method'), $id_playlist.'/'.config('guzzle.operations.tracks.name'), [
                'headers' => [
                    'Authorization' => 'Bearer '.$bearer_token
                ]
            ]);
            return $response->getBody()->getContents();
        //} catch (ConnectException $e) {
        //    throw new GuzzleHandlerException('timeout');
        //} catch (ClientException | ServerException $e) {
        //    throw new GuzzleHandlerException($e->getResponse()->getStatusCode());
        //} catch (Exception $e) {
        //    throw new GuzzleHandlerException('000');
        //} ////// Se manejan los errores que puede presentar la consulta, para mostrar distintos mensajes en el frontend

    }
}