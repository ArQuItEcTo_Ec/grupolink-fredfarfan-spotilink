<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrackImg extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ctr_tracks__imgs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_track',
        'width',
        'height',
        'url'
    ];

    public function artist()
    {
        return $this->belongsTo(Artist::class, 'id_artist');
    }
}
