<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Artist extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ctr_artists';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_str',
        'name',
        'url'
    ];

    public function getTracksAttribute()
    {
        return Track::join('ctr_artists__tracks','ctr_tracks.id', '=', 'ctr_artists__tracks.id_track')->where('ctr_artists__tracks.id_artist', $this->attributes['id'])->select('ctr_tracks.*')->get();
    }
}
