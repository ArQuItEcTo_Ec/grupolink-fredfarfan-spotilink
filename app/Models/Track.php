<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Track extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ctr_tracks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_artist',
        'id_str',
        'name',
        'url'
    ];

    public function getArtistsAttribute()
    {
        return Artist::join('ctr_artists__tracks', 'ctr_artists.id', '=', 'ctr_artists__tracks.id_artist')
            ->where('id_track', $this->attributes['id'])
            ->select('ctr_artists.*')
            ->get();
    }

    public function imgs()
    {
        return $this->hasMany(TrackImg::class, 'id_track');
    }
}
