<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Playlist extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ctr_playlists';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_str',
        'name'
    ];

    public function getTracksAttribute()
    {
        return Track::join('ctr_playlists__tracks','ctr_tracks.id', '=', 'ctr_playlists__tracks.id_track')->where('ctr_playlists__tracks.id_playlist', $this->attributes['id'])->select('ctr_tracks.*')->get();
    }
}
