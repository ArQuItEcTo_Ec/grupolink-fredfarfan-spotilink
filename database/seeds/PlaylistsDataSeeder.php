<?php

use App\Models\Playlist;
use Illuminate\Database\Seeder;

class PlaylistsDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Playlist::create([
            'id_str' => '5yYpKv2k8Asbg6IMuWoncg',
        ]);
        Playlist::create([
            'id_str' => '5uZ1Q9RGUgplLabO73Qb7w',
        ]);
    }
}
