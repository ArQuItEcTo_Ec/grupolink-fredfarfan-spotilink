<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracksByPlaylistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctr_playlists__tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_playlist');
            $table->foreign('id_playlist')->references('id')->on('ctr_playlists');
            $table->unsignedInteger('id_track');
            $table->foreign('id_track')->references('id')->on('ctr_tracks');
            $table->unsignedInteger('id_version');
            $table->foreign('id_version')->references('id')->on('ctr_playlists__versions');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctr_playlists__tracks');
    }
}
