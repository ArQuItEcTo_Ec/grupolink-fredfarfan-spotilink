<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImgsByTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctr_tracks__imgs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_track');
            $table->foreign('id_track')->references('id')->on('ctr_tracks');
            $table->integer('width')->default(0);
            $table->integer('height')->default(0);
            $table->longText('url');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctr_tracks__imgs');
    }
}
