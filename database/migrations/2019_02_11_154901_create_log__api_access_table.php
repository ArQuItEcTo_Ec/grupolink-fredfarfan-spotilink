<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogApiAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log__api_access', function (Blueprint $table) {
            $table->increments('id');
            $table->ipAddress('ip');
            $table->string('method',50)->nullable();
            $table->string('resource',255)->nullable();
            $table->string('operation',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log__api_access');
    }
}
