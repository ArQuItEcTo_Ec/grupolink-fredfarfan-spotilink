<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracksByArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctr_artists__tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_artist');
            $table->foreign('id_artist')->references('id')->on('ctr_artists');
            $table->unsignedInteger('id_track');
            $table->foreign('id_track')->references('id')->on('ctr_tracks');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctr_artists__tracks');
    }
}
