<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* RUTAS PARA LA VERSION 1 DEL API */
Route::group(['prefix'=> '/v1', 'as'=> 'v1.'], function () {

    /* RUTAS PARA EL RECURSO "TRACKS" */
    Route::group(['prefix' => '/tracks', 'as' => 'tracks.'], function() {
        Route::get('/{option?}', ['as'=>'index', 'uses'=> 'Api\TracksController@index']);
    });

    /* RUTAS PARA EL RECURSO "PLAYLIST" */
    Route::group(['prefix'=>'/playlists', 'as'=>'playlists.'], function () {
        Route::get('/', 'Api\PlaylistController@index');
        Route::get('/{playlist}/tracks', 'Api\PlaylistController@tracks');
    });

    /* RUTAS PARA EL RECURSO "ARTISTS" */
    Route::group(['prefix'=>'/artists', 'as'=>'artists.'], function () {
        Route::get('/', 'Api\ArtistsController@index');
        Route::get('/{artist}/tracks', 'Api\ArtistsController@tracks');
    });
});