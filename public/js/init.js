M.Sidenav.init(document.getElementById('nav-mobile'));
M.Sidenav.init(document.getElementById('calendar-slide-out'), {edge: 'right'});
M.Dropdown.init(document.querySelector('.dropdown-trigger'), {coverTrigger: false});
M.Dropdown.init(document.querySelector('#notificaciones-btn'), {coverTrigger: false, onOpenStart: function () { this.el.classList.add('active'); }, onCloseStart: function () { this.el.classList.remove('active');}});
M.Collapsible.init(document.querySelector('.collapsible'));
M.Parallax.init(document.querySelector('.parallax'));
M.CharacterCounter.init(document.querySelectorAll('textarea'));
M.FormSelect.init(document.querySelectorAll('select'));
M.Tabs.init(document.querySelectorAll('.tabs'), {});
M.Modal.init(document.querySelectorAll('.modal'));
M.FloatingActionButton.init(document.querySelector('.fixed-action-btn'));
M.Timepicker.init(document.querySelectorAll('.timepicker'), {twelveHour: false});
M.Tooltip.init(document.querySelectorAll('.tooltipped'), {});
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.scrollspy');
    var instances = M.ScrollSpy.init(elems, {});
});
M.Datepicker.init(document.querySelectorAll('.datepicker'), {
    firstDay: 1,
    autoClose: true,
    format: "yyyy-mm-dd",
    yearRange: 3,
    i18n: {
        cancel: "Cancelar",
        clear: "Limpiar",
        done: "Aceptar",
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        weekdaysAbbrev: ['D','L','M','M','J','V','S']
    }, /*onClose: function () {
        if(this.el.id === 'fecha_documento') {
            if(this.el.value.replace(/^\s+/g, '').length) {
                var old_date = moment(this.el.value + " -5:00", "YYYY-MM-DD ZZ");
                console.log(old_date);
                var new_date = old_date.add(3, 'days');
                console.log(new_date);
                var new_datepicktime = document.getElementById('fecha_respuesta');
                if(new_datepicktime != null) {
                    new_datepicktime.setAttribute('value', new_date.format("YYYY-MM-DD"));
                    M.updateTextFields();
                }
            }
        }
    }*/
});

