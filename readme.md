## SpotiLink

Primera versión de "SpotiLink", creado por Frederick Farfán para "una prueba de sonido que no suena" en Grupo Link.

### Autor
Frederick Farfán J. (0979711490)


### Instrucciones de instalación
- Se debe crear un archivo .env y copiar el contenido de .env.example.
- La base de datos fue creada según mi ambiente local, pero debe modificarse en el archivo .env para su correcto funcionamiento.